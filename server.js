// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.get("/supprSessions", function(req, res, next) {
	db.all('DELETE  FROM sessions;', function(err, data) {
		res.redirect('/sessions');
	});
});


//app.use(cookieParser());




app.post("/login", function(req, res, next) {
	
	/*Récupération des valeurs du formulaire*/
	var id = (typeof req.body.login !== 'undefined')? req.body.login : false;
	var mdp  = (typeof req.body.mdp !== 'undefined')? req.body.mdp : false;

	/*Authentification*/
	if(id && mdp)
	{
		db.get('SELECT * FROM users where ident = ? and password= ?',[id,mdp],function(err,data){
		
		if(typeof data !== 'undefined')
			{
				/*Création Token*/
				var ValeurToken = Math.random().toString(36).substr(2) + Date.now()+Math.random().toString(36).substr(2) + id + Math.random().toString(36).substr(2) + mdp;
				console.log(ValeurToken);
				
				
				/*Ajout du Token dans la BDD */
				db.get('SELECT ident FROM sessions where ident = ? ',[id],function(err,data){
				if(typeof data !== 'undefined')
				{
				 
					/*Modification du Token dans la BDD si l'utilisateur a déjà un token*/
				db.run('UPDATE sessions SET token = ? WHERE ident = ?',[ValeurToken,id],function(err,data){

				});

				}
				else{
				
					/*Ajout du Token dans la BDD si l'utilisateur n'as pas encore de token*/
				db.run('INSERT INTO sessions(ident,token) VALUES (?,?)',[id,ValeurToken],function(err,data){

				});

				}

				res.cookie('CookieTest',ValeurToken, { maxAge: 900000, httpOnly: true });
				
				/*Affichage */
				res.json({status: 'true' , token : ValeurToken });

				});

				
				
				
			}
		else
			{
				res.json({status: 'false'});
			}});		
	}
	else
	{
	res.json({status: 'false'});
	}

	
});







// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
